params ["_helipad"];


if ( isNil {nozoPickupPoints} ) then {
    nozoPickupPoints = [];
};

execVM "nozo\testValues.sqf";
if(2000 < (DropOffTrigger distance _helipad)) exitWith {
    false;
};

_added = false;
{
    _isClose = [_helipad,_x] call compile preprocessFileLineNumbers "nozo\LZGroup\isClose.sqf";

    if(_isClose) then {
        _added = true;

        [_helipad,_x] execVM "nozo\LZGroup\addLZPoint.sqf";

        break;
    };
}forEach nozoPickupPoints;

if(!_added) then {
    _newGroup = [_helipad] call compile preprocessFileLineNumbers "nozo\LZGroup\createLZGroup.sqf";

    nozoPickupPoints pushBack _newGroup;
};