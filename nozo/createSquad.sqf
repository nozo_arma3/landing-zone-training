params["_position"];

spawnGroup = createGroup west;

currentMission set ["group", spawnGroup];

_spawnPosition = _position getPos[20 ,random 360];

_unitType = "BWA3_Rifleman_Fleck";

spawnGroup createUnit [_unitType, _spawnPosition, [],0,"NONE" ];
spawnGroup createUnit [_unitType, _spawnPosition, [],0,"NONE" ];
spawnGroup createUnit [_unitType, _spawnPosition, [],0,"NONE" ];
spawnGroup createUnit [_unitType, _spawnPosition, [],0,"NONE" ];
spawnGroup createUnit [_unitType, _spawnPosition, [],0,"NONE" ];

spawnGroup setBehaviour "COMBAT";
spawnGroup setCombatMode "GREEN";

_pointerUnit = leader spawnGroup;

_pointerUnit enableIRLasers true;

_invisibleTarget = createVehicle["CBA_B_InvisibleTarget", [_position select 0, _position select 1, 1000],[],0,"NONE"];

_pointerUnit doTarget _invisibleTarget;
_pointerUnit doWatch [_position select 0, _position select 1, 1000];

spawnGroup deleteGroupWhenEmpty true;