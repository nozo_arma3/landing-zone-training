// create Markers for each landing zone

execVM "nozo\testValues.sqf";
if(printAllLZ == 1) then {
    _i=0;
    {
        _group = _x;

        _center = _group get "center";

        _cmarker = createMarker [ format ["LZ%1",_center], _center ];
        _cmarker setMarkerType "hd_pickup";
        _cmarker setMarkerColor "ColorOrange";

        _variation = _group get "variation";

        _area = createMarker [format ["LZArea%1",_center], _center];
        _area setMarkerShape "ELLIPSE";
        _area setMarkerColor "ColorOrange";
        _area setMarkerSize [_variation, _variation];
        _area setMarkerBrush "Border";

        _points = _group get "points";

        {
            _lzPoint = _x;
            _marker = createMarker [ format ["LZ%1",_lzPoint], _lzPoint ];
            _marker setMarkerType "hd_pickup";
            _marker setMarkerColor "ColorWhite";
            _marker setMarkerText format ["LZ %1",_i];
            _i = _i+1;
        } forEach _points;

    } forEach nozoPickupPoints;
};