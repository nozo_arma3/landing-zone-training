params [ "_lzPoint","_group"];

_points = _group get "points";
_points pushBack _lzPoint;

// calculate center
_center = [0,0];

{
    _center set [0, (_center select 0) + (_x select 0)];
    _center set [1, (_center select 1) + (_x select 1)];
}forEach _points;

_center set [0, (_center select 0) / (count _points)];
_center set [1, (_center select 1) / (count _points)];

if( count _points > 2) then {
    hint format ["Center of %1:\n%2",_points,_center];
};
// end calculate center

_group set ["center", _center];
_group set ["points", _points];
_group set ["variation", 100 / (count _points)];

_group;