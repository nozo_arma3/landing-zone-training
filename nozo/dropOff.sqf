Hint "drop off";   
_vehicle = vehicle player;    
if(_vehicle != player)then{ 
  [_vehicle] spawn { 
    params ['_vehicle']; 
    waitUntil {isTouchingGround _vehicle};   
    _group = crew _vehicle;   
   
    { unassignVehicle _x } forEach _group; 
    _group allowGetIn false; 
   
    [_group] spawn {
      sleep 120;
      params ["_group"];
      { _x hideObject true } forEach _group;
    };
	
	[] spawn {
		sleep 10;
		execVM "nozo\createPickUpMission.sqf"
	};
  } 
}