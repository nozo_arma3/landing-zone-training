params["_position"];

_smoke = currentMission get "signals";

_daytime = daytime;
_night=(_daytime < 6.5) || (_daytime > 18.5);
if(_night) then {
    _smoke pushBack (createVehicle["F_40mm_CIR", [_position select 0, _position select 1, 100],[],0,"FLY"]);
    for "_i" from 1 to 5 do {
        _smoke pushBack (createVehicle["B_IRStrobe",_position getPos[3 + random 2, _i * 72],[],0,"NONE"]);
    };
    hint "Flare out";
}
else{
    _smoke pushBack ("SmokeShellPurple" createVehicle _position);
    hint "Smoke out";
};
currentMission set ["signals", _smoke];