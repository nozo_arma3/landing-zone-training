
_positionGroupIndex = floor random count nozoPickupPoints;
_positionGroup = nozoPickupPoints select _positionGroupIndex;

_points = _positionGroup get "points";
_center = _positionGroup get "center";
_variation = _positionGroup get "variation";

_markerPosition = _center getPos [_variation * sqrt random 1, random 360];

_lzIndex = floor random count _points;
_lzPosition = _points select _lzIndex;

// hint format ["Group: %1, lz: %2\nmarker: %3, lz: %4", _positionGroupIndex, _lzIndex, _markerPosition, _lzPosition];

[_markerPosition] execVM "nozo\directionHint.sqf";

[_markerPosition,_lzPosition] execVM "nozo\spawnLandingZone.sqf";