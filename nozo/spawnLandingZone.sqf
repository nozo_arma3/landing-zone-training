params ["_markerPosition","_lzPosition"];

// Hint format ["create point %1", _markerPosition];

if(!isNil{currentMission})then {
    _group = currentMission get "group";

    deleteVehicle (currentMission get "smokeTrigger");
    deleteMarker (currentMission get "marker");
    [_group] spawn{
       params ["_group"];
       sleep 30;
       {
         deleteVehicle _x;
       } forEach (units _group);
    };
    deleteVehicle (currentMission get "landingTrigger");

    {
        deleteVehicle _x;
    }forEach (currentMission get "signals");
};

currentMission = createHashMap;

currentMission set ["signals", []];

_smokeTrigger = createTrigger ["EmptyDetector", _lzPosition];
_smokeTrigger setTriggerArea [1000, 1000, 0, false];
_smokeTrigger setTriggerActivation ["ANYPLAYER", "PRESENT", true];
_smokeTrigger setTriggerStatements [
  "this",
  "[[getPos thisTrigger select 0, getPos thisTrigger select 1]] execVM 'nozo\triggerSmoke.sqf';",
  ""
];

currentMission set ["smokeTrigger",_smokeTrigger];

_marker = createMarker [ "LZ", _markerPosition ];
_marker setMarkerType "hd_pickup";
_marker setMarkerColor "ColorRed";
_marker setMarkerText "LZ Zulu";

currentMission set ["marker",_marker];

[_lzPosition] execVM "nozo\createSquad.sqf";

_landingTrigger = createTrigger ["EmptyDetector", _lzPosition];
_landingTrigger setTriggerArea [10, 10, 1, false];
_landingTrigger setTriggerActivation ["ANYPLAYER", "PRESENT", true];
_landingTrigger setTriggerStatements [
  "this",
  "_vehicle = vehicle player;
  if(_vehicle != player)then{
	
	[_vehicle] spawn {
		params ['_vehicle'];
		waitUntil {isTouchingGround _vehicle};
		
		_group = units spawnGroup;
		
		Hint format ['%1 Units to mount',count _group];
		
		spawnGroup addVehicle _vehicle;
		{ 
		  _x assignAsCargo _vehicle;
		  [_x] orderGetIn true;  
		  
		} forEach _group;
	};
  }
  else{
    Hint 'Nope';
  }",
  ""
];

currentMission set ["landingTrigger",_landingTrigger];